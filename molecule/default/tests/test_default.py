import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_bash_profile_histtimeformat(host):
    histtimefile = host.file('/etc/profile.d/histtimeformat.sh')
    assert histtimefile.contains('export HISTTIMEFORMAT="%F %T "')
